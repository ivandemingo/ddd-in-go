# Build stage
FROM golang as builder

WORKDIR /app

COPY go.* ./
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build cmd/server/main.go

# Final stage
FROM scratch
COPY --from=builder /app/main /app/
EXPOSE 8000
ENTRYPOINT ["/app/main"]