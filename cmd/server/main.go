package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/ivandemingo/ddd-in-go/internal/infra/di"
	"gitlab.com/ivandemingo/ddd-in-go/internal/infra/handler"
)

func main() {
	di.InitializeContainer()

	e := echo.New()
	e.Use(middleware.Logger())
	e.GET("/", handler.HomeHandler)
	e.POST("/posts", handler.CreatePost)
	e.GET("/posts/:id", handler.ShowPost)
	e.Logger.Fatal(e.Start(":8000"))
}