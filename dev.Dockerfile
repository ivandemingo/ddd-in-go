FROM golang

WORKDIR /app

COPY go.* ./
RUN go mod download

RUN go get github.com/githubnemo/CompileDaemon

COPY . .

EXPOSE 8000

CMD CompileDaemon --build="go build cmd/server/main.go" --command=./main --color=true