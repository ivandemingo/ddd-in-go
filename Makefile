all: help

dc = docker-compose -f docker-compose.yml -f docker-compose.dev.yml

.PHONY: help
help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

.PHONY: start
start: ## Create and start containers for the API server
	$(dc) up

.PHONY: bg
bg: ## Create and start containers for the API server in background
	$(dc) up -d

.PHONY: build
build: ## Build or rebuild services
	$(dc) build

.PHONY: stop
stop: ## Stop containers
	$(dc) stop

.PHONY: down
down: ## Stop and remove containers
	$(dc) down