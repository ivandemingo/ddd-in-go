package post

import "gitlab.com/ivandemingo/ddd-in-go/internal/domain/post"

type Schema struct {
	Id string `json:"id"`
	Text string `json:"text"`
	Author string `json:"author"`
}

type Transformer struct {}

func New() *Transformer {
	return &Transformer{}
}

func (t *Transformer) Transform(p post.Post) Schema {
	return Schema{
		Id:     p.Id(),
		Text:   p.Text(),
		Author: p.Author(),
	}
}