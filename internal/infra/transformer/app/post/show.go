package post

import (
	post2 "gitlab.com/ivandemingo/ddd-in-go/internal/domain/post"
	"gitlab.com/ivandemingo/ddd-in-go/internal/infra/transformer/domain/post"
)

type ShowPostTransformer struct {
	transformer *post.Transformer
}

func NewShowTransformer(t *post.Transformer) *ShowPostTransformer {
	return &ShowPostTransformer{transformer: t}
}

func (s *ShowPostTransformer) Transform(p post2.Post) interface{} {
	return s.transformer.Transform(p)
}