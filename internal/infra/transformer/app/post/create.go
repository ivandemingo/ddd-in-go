package post

import (
	post2 "gitlab.com/ivandemingo/ddd-in-go/internal/domain/post"
	"gitlab.com/ivandemingo/ddd-in-go/internal/infra/transformer/domain/post"
)

type CreatePostTransformer struct {
	postTransformer *post.Transformer
}

func NewCreateTransformer(p *post.Transformer) *CreatePostTransformer {
	return &CreatePostTransformer{postTransformer: p}
}

func (c *CreatePostTransformer) Transform(p post2.Post) interface{} {
	return c.postTransformer.Transform(p)
}