package post

import (
	"gitlab.com/ivandemingo/ddd-in-go/internal/domain/post"
)

type MemoryRepository struct {
	posts map[string]*post.Post
}

func New() *MemoryRepository {
	return &MemoryRepository{
		posts: make(map[string]*post.Post),
	}
}

func (r *MemoryRepository) Save(p *post.Post) {
	r.posts[p.Id()] = p
}

func (r *MemoryRepository) FindById(id string) post.Post {
	if p, ok := r.posts[id]; ok {
		return *p
	}

	return post.Post{}
}