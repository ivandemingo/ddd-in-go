package di

import (
	"gitlab.com/ivandemingo/ddd-in-go/internal/app/post"
	post2 "gitlab.com/ivandemingo/ddd-in-go/internal/infra/persistence/post"
	post4 "gitlab.com/ivandemingo/ddd-in-go/internal/infra/transformer/app/post"
	post3 "gitlab.com/ivandemingo/ddd-in-go/internal/infra/transformer/domain/post"
)

var Container *di

type di struct {
	// Infra
	PostMemoryRepository *post2.MemoryRepository

	// Domain
	PostTransformer *post3.Transformer

	// App
	CreatePostTransformer *post4.CreatePostTransformer
	ShowPostTransformer *post4.ShowPostTransformer
	CreatePost *post.CreatePost
	ShowPost *post.ShowPost
}

func InitializeContainer() {
	Container = &di{}

	infra()
	domain()
	app()
}

/************************************************
 ******************** Infra *********************
 ************************************************/

func infra() {
	postMemoryRepository()
}

func postMemoryRepository() {
	Container.PostMemoryRepository = post2.New()
}

/************************************************
 ******************* Domain *********************
 ************************************************/

func domain() {
	postTransformer()
}

func postTransformer() {
	Container.PostTransformer = post3.New()
}

/************************************************
 ********************* App **********************
 ************************************************/

func app() {
	// Transformer
	createPostTransformer()
	showPostTransformer()

	// Use case
	createPost(Container.PostMemoryRepository, Container.CreatePostTransformer)
	showPost(Container.PostMemoryRepository, Container.ShowPostTransformer)
}

func createPostTransformer() {
	Container.CreatePostTransformer = post4.NewCreateTransformer(Container.PostTransformer)
}

func showPostTransformer() {
	Container.ShowPostTransformer = post4.NewShowTransformer(Container.PostTransformer)
}

func createPost(r *post2.MemoryRepository, t *post4.CreatePostTransformer) {
	Container.CreatePost = post.NewCreatePost(r, t)
}

func showPost(r *post2.MemoryRepository, t *post4.ShowPostTransformer) {
	Container.ShowPost = post.NewShowPost(r, t)
}
