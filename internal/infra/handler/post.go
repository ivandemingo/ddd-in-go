package handler

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/ivandemingo/ddd-in-go/internal/app/post"
	"gitlab.com/ivandemingo/ddd-in-go/internal/infra/di"
	post3 "gitlab.com/ivandemingo/ddd-in-go/internal/infra/transformer/domain/post"
	"net/http"
)

func CreatePost(c echo.Context) error {
	schema := post3.Schema{}
	c.Bind(&schema)

	r := post.CreatePostRequest{
		Id: schema.Id,
		Text: schema.Text,
		Author: schema.Author,
	}

	rp := di.Container.CreatePost.Exec(r)

	return c.JSON(http.StatusCreated, rp)
}

func ShowPost(c echo.Context) error {
	id := c.Param("id")

	r := post.ShowPostRequest{Id: id}

	rp := di.Container.ShowPost.Exec(r)

	return c.JSON(http.StatusOK, rp)
}