package post

type Post struct {
	id string
	text string
	author string
}

func New(id, text, author string) *Post {
	return &Post{
		id: id,
		text: text,
		author: author,
	}
}

func (p Post) Id() string { return p.id }
func (p Post) Text() string { return p.text }
func (p Post) Author() string { return p.author }