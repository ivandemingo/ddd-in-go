package post

type Repository interface {
	Save(p *Post)
	FindById(id string) Post
}