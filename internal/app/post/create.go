package post

import (
	"gitlab.com/ivandemingo/ddd-in-go/internal/domain/post"
)

type CreateTransformer interface {
	Transform(p post.Post) interface{}
}

type CreatePost struct {
	repo post.Repository
	transformer CreateTransformer
}

func NewCreatePost(repo post.Repository, transformer CreateTransformer) *CreatePost {
	return &CreatePost{
		repo: repo,
		transformer: transformer,
	}
}

func (c *CreatePost) Exec(r CreatePostRequest) interface{} {
	p := post.New(r.Id, r.Text, r.Author)

	c.repo.Save(p)

	return c.transformer.Transform(*p)
}

type CreatePostRequest struct {
	Id string
	Text string
	Author string
}
