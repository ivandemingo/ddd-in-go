package post

import (
	"gitlab.com/ivandemingo/ddd-in-go/internal/domain/post"
)

type ShowTransformer interface {
	Transform(p post.Post) interface{}
}

type ShowPost struct {
	repo post.Repository
	transformer ShowTransformer
}

func NewShowPost(r post.Repository, t ShowTransformer) *ShowPost {
	return &ShowPost{
		repo: r,
		transformer: t,
	}
}

func (s *ShowPost) Exec(r ShowPostRequest) interface{} {
	p := s.repo.FindById(r.Id)

	return s.transformer.Transform(p)
}

type ShowPostRequest struct {
	Id string
}